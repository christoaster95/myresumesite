let resumeData = {
    "imagebaseurl":"https://github.com/chrisssthomas",
    "name": "Chris Thomas",
    "role": "Frontend Developer and Agile Project Manager",
    "linkedinId":"Your LinkedIn Id",
    "skypeid": "Your skypeid",
    "roleDescription": " I'm originally from the pretty place you see behind this text, County Wicklow, Ireland. I moved to Manchester in 2016 with hopes of furthering my education and experience, and so far it has been an amazing journey, that I am looking forward to continuing!",
    "socialLinks":[
        {
          "name":"linkedin",
          "url":"https://www.linkedin.com/in/chris-thomas-2a73a5173/",
          "className":"fa fa-linkedin"
        },
        {
          "name":"github",
          "url":"http://github.com/chrisssthomas",
          "className":"fa fa-github"
        },
        {
          "name":"skype",
          "url":"https://twitter.com/SamohtSirhc_",
          "className":"fa fa-twitter"
        }
      ],
    "aboutme":"I have hands on experience implementing Agile in both structured corporate environments and laid back start-up environments. I am a PMI certified Scrum Master, but my skills in agile do not stop there. I have experience implementing traditional Scrum and Sprint metholodologies, as well as Kanban, Scrumban & dabbling with other lean principles. I am quite open when it comes to methodologies as long as the core principles of Agile are adhered to. I also am studying Prince2, and hope to be Prince2 Foundation certified this year. Aside from my Project Management expertise I am also a self-taught front end developer, hoping to add the last strings to my bow to become truly full-stack, proficient with technologies such as ReactJS, NodeJS, Python, Kotlin, PHP & MAX/MSP",
    "address":"Manchester, England",
    "website":"https://github.com/chrisssthomas",
    "education":[
      {
        "UniversityName":"Bray Institute of Further Education",
        "specialization":"Information Technology and Networking",
        "CertificationStandard":"QQI Level 5 & 6",
        "MonthOfPassing":"Aug",
        "YearOfPassing":"2020",
        "Achievements":"Achieved a merit and distinction respectively over my two years here. My classes involved full stack development including front end web development classes, which included PHP, Vanilla JS, HTML, CSS, with brief introduction to JS libraries such as jQuery. Data entry and database management, systems development including uses of Visual Basic & Python. Infrastructure classes which included heavy use of Windows Server, and learning core computer science principles and how to work with physical servers and internal networks. We also had other classes to refine other skills including Maths for Programming. "
      },
      {
        "UniversityName":"The Knowledge Academy",
        "specialization":"Scrum Master Certification",
        "CertificationStandard":"PMI/PMP",
        "MonthOfPassing":"Mar",
        "YearOfPassing":"2019",
        "Achievements":"I decided to test my well versed knowledge of Scrum and Agile. My employer at the time was keen to see me continue my development and financed my study and certification."
      }
    ],
    "work":[
      {
        "CompanyName":"Mercarto",
        "specialization":"Project Manager",
        "MonthOfStarting":"Oct",
        "YearOfStarting":"2019",
        "MonthOfLeaving":"Jan",
        "YearOfLeaving":"2020",
        "Achievements":"I had a great time at Mercarto and gained really valuable experience and knowledge and learned from people with an incredible tech stack, and of course working in the UKFast incubator for the former directors of Social Chain was an amazing learning experience.  From implementing aspects of various methodologies such as Scrum & Kanban, also bringing structure and initial documentation to clients and internal stakeholders. Providing performance data through Jira and Confluence. Working with Developers day to day to solve their impedements and increase productivity by promoting a balance of autonomy and alignment, in which the developers soon began to thrive. Bringing this group of developers from no documentation and basic Jira use to proficiency and engaging with the process in an extremely short time. Bringing in positive Agile ceromonies such as Sprint planning and Sprint retrospectives, and keeping things light and fun so that people don t feel bored or unmotivated. Focusing on bringing stand ups and development meetings to be dev only meetings increased cross-pollination and a feeling of security away from stakeholders. I left my position here to look for a more structured Agile environment in which to grow, learn and execute positive change. I am still on excellent terms with Sean Brown (CEO) & all of the staff"
      },
      {
        "CompanyName":"Some Company",
        "specialization":"Some specialization",
        "MonthOfLeaving":"Jan",
        "YearOfLeaving":"2018",
        "Achievements":"Some Achievements"
      }
    ],
    "skillsDescription":"Below you will find a visual representation for my own valuation of my profiency in various technologies and engineering methodologies.",
    "skills":[
      {
        "skillname":"HTML5"
      },
      {
        "skillname":"CSS"
      },
      {
        "skillname":"Reactjs"
      },
      {
        "skillname":"JavaScript"
      },
      {
        "skillname":"NodeJS"
      },
      {
        "skillname":"Python"
      },
      {
        "skillname":"Kotlin"
      },
      {
        "skillname":"MongoDB"
      },
      {
        "skillname":"MySQL"
      }
    ],
    "portfolio":[
      {
        "name":"Pizza Finder",
        "description":"A site I made when first learning development, still really enjoy the designs and implementation",
        "imgurl":"images/portfolio/coffee.jpg"
      },
      {
        "name":"project2",
        "description":"mobileapp",
        "imgurl":"images/portfolio/farmerboy.jpg"
      },
      {
        "name":"project3",
        "description":"mobileapp",  
        "imgurl":"images/portfolio/judah.jpg"
      },
      {
        "name":"project4",
        "description":"mobileapp",
        "imgurl":"images/portfolio/origami.jpg"
      }
    ],
    "testimonials":[
      {
        "description":"This is a sample testimonial",
        "name":"Some technical guy"
      },
      {
        "description":"This is a sample testimonial",
        "name":"Some technical gfdsdffsuy"
      }
    ]
  }
  
  export default resumeData